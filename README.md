# Projet SpendAll

## Description
Ce projet est une application Angular conçue pour gérer les transactions financières.

## Prérequis
- Node.js
- Angular CLI

## Installation
1. Tout d'abord, clonez le projet depuis GitLab : git clone https://gitlab.com/flojarno/spend-all.git
2. Naviguez vers le répertoire du projet : cd spend-all
3. Installez les dépendances : npm install

## Lancer l'application
Pour démarrer l'application, exécutez : ng serve --open

Cela ouvrira automatiquement l'application dans votre navigateur web par défaut à l'adresse `http://localhost:4200/`.
